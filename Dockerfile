# pull official base image
FROM python:3.6.9-alpine

# set work directory
RUN mkdir /usr/src/backend_django
WORKDIR /usr/src/backend_django

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# install dependencies
RUN pip install --upgrade pip
COPY ./backend_django/requirements.txt /usr/src/backend_django/requirements.txt
RUN pip install -r requirements.txt

RUN mkdir /usr/src/frontend_static